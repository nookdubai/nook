nook is the first co-working space dedicated to helping sports, fitness and wellness industry professionals set up and run their own businesses. Located in The One, JLT - within the Dubai Multi Commodities Centre (DMCC) free zone - nook provides everything a professional needs to set up a business in the UAE.

Address : One JLT, JLT, Dubai 1111, UAE

Phone : +971 4 447 2394
